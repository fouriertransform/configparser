/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>

#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>

#define private public

#include <include/reader.hpp>

using namespace parser;

#define PARSE(statement) \
    out.clear();     \
    REQUIRE_NOTHROW((out = Reader((statement)).Parse().GetContainer()));

#define PARSE_EX(statement, text) \
    REQUIRE_THROWS_WITH(Reader((statement)).Parse(), text);

#define CHECK_TYPE(var, var_type) \
    REQUIRE(out[var].m_type == var_type);

TEST_CASE("Basic type tests", "[reader]") {
    Container out;

    PARSE("ex = true ; bool")
    CHECK_TYPE("ex", kBool)

    PARSE("ex = 1.23 ; float")
    CHECK_TYPE("ex", kFloat)

    PARSE("ex = 123 ; int")
    CHECK_TYPE("ex", kInt)

    PARSE(R"(ex = "one two three" ; string)")
    CHECK_TYPE("ex", kString)

    PARSE("ex = [1, 2, 3] ; array<int>")
    CHECK_TYPE("ex", kArray)
}

TEST_CASE("IntegerType tests", "[reader]") {
    Container out;

    SECTION("Positive value") {
        SECTION("Round to lower part") {
            PARSE("ex = 13.37 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == 13);
        }

        SECTION("Round to higher part") {
            PARSE("ex = 133.7 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == 134);
        }

        SECTION("Correct value") {
            PARSE("ex = 1337 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == 1337);
        }

        SECTION("Maximum value") {
            auto max = std::numeric_limits<IntegerType>::max();
            PARSE("ex = " + std::to_string(max) + " ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == max);
        }

        SECTION("Overflow") {
            auto max = std::numeric_limits<IntegerType>::max();
            PARSE("ex = " + std::to_string(max + 1) + " ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == (max + 1));
        }
    }

    SECTION("Negative value") {
        SECTION("Round to lower part") {
            PARSE("ex = -13.37 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == -13);
        }

        SECTION("Round to higher part") {
            PARSE("ex = -133.7 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == -134);
        }

        SECTION("Correct value") {
            PARSE("ex = -1337 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == -1337);
        }

        SECTION("Minimum value") {
            auto min = std::numeric_limits<IntegerType>::min();
            PARSE("ex = " + std::to_string(min) + " ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == min);
        }

        SECTION("Underflow") {
            auto min = std::numeric_limits<IntegerType>::min();
            PARSE("ex = " + std::to_string(min - 1) + " ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == (min - 1));
        }
    }

    SECTION("Null value") {
        SECTION("Correct value") {
            PARSE("ex = 0.00000 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == 0);
        }

        SECTION("Correct value") {
            PARSE("ex = 0 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == 0);
        }

        SECTION("Correct value") {
            PARSE("ex = -0.00000 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == 0);
        }

        SECTION("Correct value") {
            PARSE("ex = -0 ; int")
            REQUIRE(out["ex"].Get<IntegerType>() == 0);
        }
    }
}

TEST_CASE("FloatType tests", "[reader]") {
    Reader config{ {} };
    Container out;

    SECTION("Positive value") {
        SECTION("Correct value") {
            PARSE("ex = 1337.1337 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == Approx(1337.1337));
        }

        SECTION("Correct value (with 9 decimal places)") {
            PARSE("ex = 0.142857142 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == Approx(0.142857142));
        }

        SECTION("Correct value (with 17 decimal places; should ignore extra digits)") {
            PARSE("ex = 0.142857142 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == Approx(0.142857142));
        }

        SECTION("Maximum value") {
            auto max = std::numeric_limits<FloatType>::max();
            PARSE("ex = " + std::to_string(max) + " ; float")
            REQUIRE(out["ex"].Get<FloatType>() == Approx(max));
        }

        SECTION("Overflow") {
            auto max = std::numeric_limits<FloatType>::max();
            PARSE("ex = " + std::to_string(max + 1) + " ; float")
            REQUIRE(out["ex"].Get<FloatType>() == Approx(max + 1));
        }
    }

    SECTION("Negative value") {
        SECTION("Correct value") {
            PARSE("ex = -1337.1337 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == Approx(-1337.1337));
        }

        SECTION("Minimum value") {
#if (FloatType == float)
            PARSE("ex = -340282300000000000000000000000000000000 ; float");
            REQUIRE(out["ex"].Get<FloatType>() == -340282300000000000000000000000000000000.f);
#else
            PARSE( "ex = -17976931348623158000000000000000000000000"
                   "00000000000000000000000000000000000000000000000000000000"
                   "00000000000000000000000000000000000000000000000000000000"
                   "00000000000000000000000000000000000000000000000000000000"
                   "00000000000000000000000000000000000000000000000000000000"
                   "00000000000000000000000000000000000000000000 ; float" )
            REQUIRE( out[ "ex" ].Get< FloatType >() == -179769313486231580000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0 );
#endif //
        }

        SECTION("Underflow") {
            auto min = std::numeric_limits<FloatType>::min();
            PARSE("ex = " + std::to_string(min - 1) + " ; float")
            REQUIRE(out["ex"].Get<FloatType>() == Approx(min - 1));
        }
    }

        // don't use approximations here
    SECTION("Null value") {
        SECTION("Correct value") {
            PARSE("ex = 0.00000 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == 0);
        }

        SECTION("Correct value") {
            PARSE("ex = 0 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == 0);
        }

        SECTION("Correct value") {
            PARSE("ex = -0.00000 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == 0);
        }

        SECTION("Correct value") {
            PARSE("ex = -0 ; float")
            REQUIRE(out["ex"].Get<FloatType>() == 0);
        }
    }
}

TEST_CASE("boolean tests", "[reader]") {
    Reader config{ {} };
    Container out;

    SECTION("True") {
        PARSE("ex = true ; bool")
        REQUIRE(out["ex"].Get<bool>() == true);

        PARSE("ex = 1 ; bool")
        REQUIRE(out["ex"].Get<bool>() == true);
    }

    SECTION("False") {
        PARSE("ex = false ; bool")
        REQUIRE(out["ex"].Get<bool>() == false);
    }

    SECTION("Case-sensitive") {
        PARSE("ex = True ; bool")
        REQUIRE(out["ex"].Get<bool>() == false);

        PARSE("ex = TRUE ; bool")
        REQUIRE(out["ex"].Get<bool>() == false);
    }
}

TEST_CASE("StringType tests", "[reader]") {
    Reader config{ {} };
    Container out;

    SECTION("Correct value") {
        PARSE(R"(ex = "true" ; string)")
        REQUIRE(out["ex"].Get<std::string>() == "true");

        PARSE(R"(ex = "one two three" ; string)")
        REQUIRE(out["ex"].Get<std::string>() == "one two three");

        PARSE(R"(ex = "!@#$%^&**().,;'[]\`\"\\"" ; string)")
        REQUIRE(out["ex"].Get<std::string>() == R"(!@#$%^&**().,;'[]\`"\")");
    }

    SECTION("Incorrect value") {
        using namespace Catch;
        PARSE_EX(R"(ex = ")", Contains("Unterminated string"));
        PARSE_EX(R"(ex = !"123" ; string)", Contains("Unexpected character encountered"));
    }
}

TEST_CASE("ArrayType tests", "[reader]") {
    Container out;

    const auto get_subtype = [](ArrayType* arr) {
        return arr->empty() ? kUndefined : arr->at(0).m_type;
    };

    SECTION("Bool subtype") {
        PARSE("ex = [true, true, false] ; array< bool >")
        REQUIRE(out["ex"].m_type == kArray);

        auto* arr = out["ex"].m_value.m_array;
        REQUIRE(arr != nullptr);
        REQUIRE(arr->size() == 3);
        REQUIRE(arr->at(0).Get<bool>() == true);
        REQUIRE(arr->at(1).Get<bool>() == true);
        REQUIRE(arr->at(2).Get<bool>() == false);
        REQUIRE(get_subtype(arr) == kBool);
    }

    SECTION("Float subtype") {
        PARSE("ex = [13.37, 3.14, 3.14159, 7.0] ; array< float >")
        REQUIRE(out["ex"].m_type == kArray);

        auto* arr = out["ex"].m_value.m_array;
        REQUIRE(arr != nullptr);
        REQUIRE(arr->size() == 4);
        REQUIRE(arr->at(0).Get<FloatType>() == Approx(13.37));
        REQUIRE(arr->at(1).Get<FloatType>() == Approx(3.14));
        REQUIRE(arr->at(2).Get<FloatType>() == Approx(3.14159));
        REQUIRE(arr->at(3).Get<FloatType>() == Approx(7.0));
        REQUIRE(get_subtype(arr) == kFloat);
    }

    SECTION("Int subtype") {
        PARSE("ex = [1, 3, 3, 7] ; array< int >")
        REQUIRE(out["ex"].m_type == kArray);

        auto* arr = out["ex"].m_value.m_array;
        REQUIRE(arr != nullptr);
        REQUIRE(arr->size() == 4);
        REQUIRE(arr->at(0).Get<IntegerType>() == 1);
        REQUIRE(arr->at(1).Get<IntegerType>() == 3);
        REQUIRE(arr->at(2).Get<IntegerType>() == 3);
        REQUIRE(arr->at(3).Get<IntegerType>() == 7);
        REQUIRE(get_subtype(arr) == kInt);
    }

    SECTION("String subtype") {
        PARSE(R"(ex = ["one", "two", "three", "four"] ; array< string >)")
        REQUIRE(out["ex"].m_type == kArray);

        auto* arr = out["ex"].m_value.m_array;
        REQUIRE(arr != nullptr);
        REQUIRE(arr->size() == 4);
        REQUIRE(arr->at(0).Get<std::string>() == "one");
        REQUIRE(arr->at(1).Get<std::string>() == "two");
        REQUIRE(arr->at(2).Get<std::string>() == "three");
        REQUIRE(arr->at(3).Get<std::string>() == "four");
        REQUIRE(get_subtype(arr) == kString);
    }

    SECTION("Multiline test without comments") {
        std::string content = R"(
ex = [
    "one",
    "two",
    "three",
    "four"
] ; array<string>
)";
        PARSE(content)
        REQUIRE(out["ex"].m_type == kArray);

        auto* arr = out["ex"].m_value.m_array;
        REQUIRE(arr != nullptr);
        REQUIRE(arr->size() == 4);
        REQUIRE(arr->at(0).Get<std::string>() == "one");
        REQUIRE(arr->at(1).Get<std::string>() == "two");
        REQUIRE(arr->at(2).Get<std::string>() == "three");
        REQUIRE(arr->at(3).Get<std::string>() == "four");
        REQUIRE(get_subtype(arr) == kString);
    }

    SECTION("Multiline test with comments between each line") {
        std::string content = R"(
ex = [
    # this is a comment 1
    "one",
    # this is a comment 2
    "two",
    # this is a comment 3
    "three",
    # this is a comment 4
    "four"
] ; array<string>
)";
        PARSE(content)
        REQUIRE(out["ex"].m_type == kArray);

        auto* arr = out["ex"].m_value.m_array;
        REQUIRE(arr != nullptr);
        REQUIRE(arr->size() == 4);
        REQUIRE(arr->at(0).Get<std::string>() == "one");
        REQUIRE(arr->at(1).Get<std::string>() == "two");
        REQUIRE(arr->at(2).Get<std::string>() == "three");
        REQUIRE(arr->at(3).Get<std::string>() == "four");
        REQUIRE(get_subtype(arr) == kString);
    }
}

TEST_CASE("operator[]", "[value]") {
    Container out;
    PARSE(R"(ex = ["one", "two", "three", "four"] ; array< string >)")
    REQUIRE(out["ex"].m_type == kArray);
    REQUIRE(out["ex"].GetSize() == 4);
    REQUIRE(out["ex"][0]->Get<std::string>() == "one");
    REQUIRE(out["ex"][1]->Get<std::string>() == "two");
    REQUIRE(out["ex"][2]->Get<std::string>() == "three");
    REQUIRE(out["ex"][3]->Get<std::string>() == "four");
}

TEST_CASE("exceptions", "[reader]") {
    using namespace Catch;
    PARSE_EX("ex 123 ; int", Contains("Expected assignment"))
    PARSE_EX("= 123 ; int", Contains("Expected variable"))
    PARSE_EX("ex = ; int", Contains("Expected value"))
}

TEST_CASE("ArrayType exceptions", "[reader]") {
    using namespace Catch;
    PARSE_EX("ex = [1, 2, 3] ; array", Contains("The array type requires open/close brackets"))
    PARSE_EX("ex = [1, 2, 3] ; ", Contains("Expected type"))
    PARSE_EX("ex = 123 ; array", Contains("Variable was defined as an array, but no array opening bracket"))
    PARSE_EX("ex = [1] ; int", Contains("Received an array-type value, instead of defined"))
    PARSE_EX("ex = [1] ; array><int>", Contains("Array close tag can't come before an open tag"))
    PARSE_EX("ex = [1] ; array<000>", Contains("Invalid array value type received"))
    PARSE_EX("ex = [1] ; array<<int>", Contains("Uneven number of open/close brackets on array type"))
    PARSE_EX("ex = [1] ; array<int>>", Contains("Array type has already been fully closed"))
    PARSE_EX("ex = [1] ; array<!!!>", Contains("Unexpected character encountered"))
    PARSE_EX("ex = [1] ; array<array<int>>", Contains("Mismatch between actual value and declared type"))
    PARSE_EX(
        "ex = [ [ [1], [2] ], [ [1, 2], [2, 3] ] ] ; array<array<array<int>>>",
        Contains("Inconsistent array sizes on a given depth aren't allowed")
    )
    PARSE_EX(
        "ex = [ [ [ [1], [2] ], [ [1, 2] ] ] ] ; array<array<array<array<int>>>>",
        Contains("Inconsistent array sizes on a given depth aren't allowed")
    )
}

TEST_CASE("Inter-op convenience tests", "[value]") {
    Container out;
    out["ex"] = 1.2f;

    REQUIRE(out["ex"].m_type == kFloat);

    Value a;
    a = false;

    REQUIRE(a.m_type == kBool);
    REQUIRE(a.Get<bool>() == false);

    a = true;

    REQUIRE(a.m_type == kBool);
    REQUIRE(a.Get<bool>() == true);

    a = 123;
    REQUIRE(a.m_type == kInt);
    REQUIRE(a.Get<IntegerType>() == 123);

    a = "123";
    REQUIRE(a.m_type == kString);
    REQUIRE(a.Get<std::string>() == "123");

    a = std::vector<Value>{ 1, 2, 3 };

    REQUIRE(a.m_type == kArray);
    REQUIRE(a.GetSize() == 3);
}

TEST_CASE("Hex value tests", "[reader]") {
    Container out;

    SECTION("Valid values") {
        PARSE("ex = 0xd34db33f ; int")
        REQUIRE(out["ex"].m_type == kInt);
        REQUIRE(out["ex"].Get<IntegerType>() == static_cast< int >( 0xd34db33f ));
    }

    SECTION("Invalid values") {
        using namespace Catch;
        PARSE_EX("ex = 0xd34db33f1 ; int", Contains("Hex value cannot have more than"))
    }
}
