ConfigParser
---
ConfigParser is a lightweight parser for a simple and intuitive key/value/type configuration format.

Essentially, the syntax boils down to:
```cpp
key = value ; type
```

Here are a few more practical examples:
```cpp
ex.b = true  ; bool
ex.i = 3456  ; int
ex.s = "123" ; string
ex.f = 78.91 ; float
ex.a = [123] ; array<int>
```

Key is an arbitrary alphanumeric string (including special characters) which doesn't contain whitespace.
Value is an arbitrary value which must conform to a correct representation for the specified type, i.e. strings need to be quoted, arrays have to be wrapped, etc.

## Documentation
Documentation is available at https://fouriertransform.bitbucket.io/ConfigParser/

## Types
| Architecture | Type   | Size            | Limits                                                      |
|--------------|--------|-----------------|-------------------------------------------------------------|
| 32/64-bit    | bool   | 1 byte          | `false` or `true`                                           |
| 32-bit       | int    | 4 bytes         | `-2,147,483,648` to `2,147,483,647`                         |
| 32-bit       | float  | 4 bytes         | `3.4E +/- 38` (7 digits)                                    |
| 64-bit       | int    | 8 bytes         | `-9,223,372,036,854,775,808` to `9,223,372,036,854,775,807` |
| 64-bit       | float  | 8 bytes         | `1.7E +/- 308` (15 digits)                                  |
| 32/64-bit    | string | variable-length | explicitly unrestricted                                     |
| 32/64-bit    | array  | variable-length | explicitly unrestricted                                     |

In any case, the internal storage of a single `Value` object is guaranteed to be at most `sizeof(void*)` bytes.
(Minus the heap allocated storage for strings and arrays, in case they're used)

## Usage
Simply put the contents of the `include/` directory inside your project
and include `reader.hpp` or `writer.hpp` as needed.

## Notes
- A sub-type value must be explicitly defined for arrays, e.g. `array<int>`, `array<string>`...
- Integers can be represented in base-16 (hexadecimal), but will nevertheless ultimately be represented in base-10
  (and written to configs as such)
- If by some mistake, a decimal point is present in an integer value, it will be parsed as a float, rounded accordingly
  and stored as an int.
- Space is optional, e.g. the following examples are all equivalent:
  + `ex.var=1;int`
  + `ex.var = 1;int`
  + `ex.var = 1 ; int`
- Everything is case-sensitive, i.e. `ex.var` is not equal to `ex.Var` (nor `EX.var` for that matter)
- Booleans evaluate to true only if their value is equal to `true` (case-sensitive) or `1`.
  They do not require a strict definition (i.e. any arbitrary value will evaluate to false).
- Do not assign your own pointers to `ValueContainer::m_array` or `ValueContainer::m_string`
  nor call free on them by yourself. Use the intended/provided constructors.
- You can configure your own floating point/integer types in `include/detail/config.hpp`, but make sure to update
  `include/detail/value_helper.hpp` accordingly.
- No tracking is performed on comments, hence comments are not saved/written at all
- Strings must be enclosed/surrounded with quotation marks
- Inside of strings, you can escape the quotation mark character using a backslash
- Since recursion is used for parsing arrays, it is not recommended to use large multidimensional arrays,
since this could lead to stack-smashing
- No proper UTF8/16/32 support. ASCII-only.

## Barebones example
```cpp
#include <fstream>
#include <iostream>

#include "parser/reader.hpp"
#include "parser/writer.hpp"

int main( int, char** ) {
    using namespace parser;

    // Define an example config
    std::string buffer =
            R"(# This is a comment :))" "\n"
            R"(ex.b    =  true ; bool # This is an additional comment)" "\n"
            R"(ex.i    =    11 ; int)" "\n"
            R"(ex.s    = "123" ; string)" "\n"
            R"(ex.f    =  3.14 ; float)" "\n"
            R"(ex.arr  = ["123", "test \"escape\"", "456" ] ; array< string >)";

    // Read our input buffer
    auto reader = Reader( buffer );

    auto container = reader.Parse().GetContainer();
    container[ "ex.new_str" ] = "123"; // Add a new string value

    Value pi;
    pi = 3.14159f;

    container[ "pi" ] = pi;

    // Print values from our container
    for( const auto& v : container )
        std::cout << v.first << ' ' << v.second.GetValueString() << std::endl;

    std::ofstream output_stream( "output.txt", std::ios::out );

    // Save buffer to output.txt
    auto adapter = OutputStreamAdapter( &output_stream );
    auto writer = Writer( &adapter );
    if( !writer.Write( container ) )
        std::cout << "Failed to write to output.txt" << std::endl;

    return EXIT_SUCCESS;
}
```

Console output (formatted for easier reading):
```
pi         3.141590
ex.new_str "123"
ex.arr     ["123", "test "escape"", "456"]
ex.s       "123"
ex.f       3.140000
ex.i       11
ex.b       true
```

File output (formatted for easier reading):
```
pi         = 3.141590 ; float
ex.new_str = "123" ; string
ex.arr     = ["123", "test \"escape\"", "456"] ; array<string>
ex.s       = "123" ; string
ex.f       = 3.140000 ; float
ex.i       = 11 ; int
ex.b       = true ; bool
```

## License
This project is licensed under GNU GPLv3. See the **COPYING** file for more information.
