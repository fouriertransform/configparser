/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_READER_HPP
#define CONFIGPARSER_READER_HPP

#pragma once

#include <functional>

#include "detail/value.hpp"
#include "detail/value_helper.hpp"
#include "detail/scanner.hpp"
#include "detail/array_parser.hpp"

namespace parser {
namespace detail {
/**
 * @brief Describes the internal state of the parser.
 * @note Parser syntax looks like this:
 *  Variable = value ; type
 *           ^       ^
 *           |    delimiter
 *       assignment
 */
enum ParserState : std::uint8_t {
    kState_Variable,
    kState_Assignment,
    kState_Value,
    kState_Delimiter,
    kState_Type
};

/**
 * @brief Denotes the length/size of @ref ParserState
 */
constexpr auto kParserStateLength = kState_Type + 1;
}

/**
 * @brief The Reader class, where all the magic happens.
 */
class Reader {
private:
    /**
     * @brief Type definitions for matching tokens.
     * If a token is matched, true is returned, otherwise false.
     */
    using MatchCallback_t = std::function<bool(const Token&)>;

    // Pointer to the lexer/scanner
    Scanner m_scanner;

    // Container keeping the parsed values
    Container m_container;

    // Indicates the current parser state
    detail::ParserState m_state = detail::kState_Variable;

private:

    /**
     * @brief Checks if the given token represents an identifier.
     * @param token Token whose type is checked.
     * @return Returns true if the token type represents an identifier, otherwise false.
     */
    static auto MatchIdentifier(const Token& token) -> bool {
        return token == TokenType::kIdentifier;
    }

    /**
     * @brief Checks if the given token represents an assignment.
     * @param token Token whose type is checked.
     * @return Returns true if the token type represents an assignment, otherwise false.
     */
    static auto MatchAssignment(const Token& token) -> bool {
        return token == TokenType::kEqual;
    }

    /**
     * @brief Checks if the given token represents a value.
     * @param token Token whose type is checked.
     * @return Returns true if the token type represents a value, otherwise false.
     */
    static auto MatchValue(const Token& token) -> bool {
        switch (token.GetType()) {
        case TokenType::kString:
        case TokenType::kNumber:
        case TokenType::kLeftBracket:
        case TokenType::kTrue:
        case TokenType::kFalse:
            return true;
        default:
            return false;
        }
    }

    /**
     * @brief Checks if the given token represents a semicolon.
     * @param token Token whose type is checked.
     * @return Returns true if the token type represents a semicolon, otherwise false.
     */
    static auto MatchSemicolon(const Token& token) -> bool {
        return token == TokenType::kSemicolon;
    }

    /**
     * @brief Expects to consume a token using the given callback.
     * @param match A callback to match the token.
     * @param error_message The error message to be thrown
     * @return The value that corresponds to the matched token, i.e. the lexeme.
     * @throws ParserError Thrown if the expected callback fails to match.
     */
    auto Expect(
        const MatchCallback_t& match, const std::string& error_message
    ) -> Token {
        // Run provided callback. If the callback fails
        // then throw an error, since the expected token
        // (according to internal parser state) doesn't
        // conform to the appropriate syntax.
        auto token = m_scanner.GetToken();
        if (!match(token))
            throw detail::ParserError(error_message, token.GetLine());

        // Update parser state on match
        m_state = static_cast< detail::ParserState >(
            (m_state + 1) % detail::kParserStateLength
        );

        // Trim the lexeme/value of the scanner token
        token.SetLexeme(detail::Trim(token.GetLexeme()));

        // Skip current token and move on to the next one
        m_scanner.Advance();

        // Remove whitespace surrounding the token lexeme, and return it as-is
        return token;
    }

    /**
     * @brief Adds a fully parsed statement to the output container.
     * @param name The name of this variable.
     * @param value The value of this variable, as a string, which will be converted
     * according to the provided @p type in this function.
     * @param type The supposed type of this variable according to the parsed statement.
     * @throws ParserError Thrown if invalid @p type is provided.
     */
    auto Add(
        const Token& name_token, const std::string& value, const ValueType type
    ) -> void {
        using detail::ValueHelper;

        const auto& name = name_token.GetLexeme();
        switch (type) {
        case kFloat:
            m_container.insert_or_assign(name, ValueHelper::GetFloat(value));
            break;
        case kInt:
            m_container.insert_or_assign(name, ValueHelper::GetInt(value));
            break;
        case kBool:
            m_container.insert_or_assign(name, ValueHelper::GetBool(value));
            break;
        case kString:
            m_container.insert_or_assign(name, value);
            break;
        default:
            throw detail::ParserError(
                "Failed to convert value based on token type for `" + name + "`", name_token.GetLine()
            );
        }
    }

    /**
     * @brief Consumes an array statement.
     * @param [in] value The token indicating the value of this statement, as per the syntax specification.
     * @param [in] name The token indicating the name of this variable statement, as per the syntax specification.
     */
    auto ConsumeArrayStatement(const Token& value, const Token& name) -> void {
        using detail::ValueHelper;
        ArrayParser array_parser(&m_scanner);

        ArrayType scanned_array;
        array_parser.ConsumeArrayValue(scanned_array);

        (void) Expect(&MatchSemicolon, "Expected semicolon");

        const auto type_token = Expect(&MatchIdentifier, "Expected type").GetLexeme();
        const auto type = ValueHelper::FromString(type_token);

        if (type != kArray) {
            throw detail::ParserError(
                "Received an array-type value, instead of defined `" + type_token + "`", value.GetLine()
            );
        }

        auto array_value = array_parser.GetArrayValue(&scanned_array);
        m_container.insert_or_assign(name.GetLexeme(), array_value);
    }

    /**
     * @brief Consumes a regular ("trivial" type) statement.
     * @param [in] value The token indicating the value of this statement, as per the syntax specification.
     * @param [in] name The token indicating the name of this variable statement, as per the syntax specification.
     */
    auto ConsumeRegularStatement(const Token& value, const Token& name) -> void {
        using detail::ValueHelper;
        (void) Expect(&MatchSemicolon, "Expected semicolon");

        const auto type_token = Expect(&MatchIdentifier, "Expected type").GetLexeme();
        const auto type = ValueHelper::FromString(type_token);

        switch (type) {
        case kArray:
            throw detail::ParserError(
                "Variable was defined as an array, but no array opening bracket ('[') was found", value.GetLine()
            );
        default:
            Add(name, value.GetLexeme(), type);
            break;
        }
    }

    /**
     * @brief Consumes a statement (or rather a line) according to the already-determined syntax.
     */
    auto ConsumeStatement() -> void {
        const auto name = Expect(&MatchIdentifier, "Expected variable");
        (void) Expect(&MatchAssignment, "Expected assignment");

        const auto value = Expect(&MatchValue, "Expected value");
        switch (value.GetType()) {
        case TokenType::kLeftBracket:
            return ConsumeArrayStatement(value, name);
        default:
            return ConsumeRegularStatement(value, name);
        }
    }

public:
    /**
     * @brief Constructs the @ref Parser class and initializes the token scanner.
     * @param scanner The tokenizer interface.
     */
    explicit Reader(std::string_view input_text) : m_scanner(input_text) {
    }

    /**
     * @brief Iterates the internal tokenizer interface until end of file and parses it along the way.
     */
    auto Parse() -> Reader& {
        while (m_scanner.GetType() != TokenType::kEOF)
            ConsumeStatement();
        return *this;
    }

    /**
     * @brief Helper interface to access the parsed data.
     */
    [[nodiscard]] auto GetContainer() const -> const Container& {
        return m_container;
    }
};
}

#endif // !CONFIGPARSER_READER_HPP
