/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_WRITER_HPP
#define CONFIGPARSER_WRITER_HPP

#pragma once

#include <vector>
#include <map>
#include <cmath>
#include <memory>
#include <fstream>

#include "detail/config.hpp"
#include "detail/value_helper.hpp"
#include "detail/value.hpp"

/**
 * @brief The main namespace in which the parser is nested.
 */
namespace parser {
struct IOutputAdapter {
    /**
     * @brief Writes the given buffer to a file.
     * @param [in] buffer A string to write to the file.
     * @return true, if the write was successful.
     * @return false, if the write was not successful.
     */
    virtual bool Write(const std::string& buffer) = 0;
};

class OutputFileAdapter : public IOutputAdapter {
private:
    std::FILE* m_file = nullptr;

public:
    /**
     * @brief Initializes this @ref OutputFileAdapter instance.
     * @param [in,out] file Pointer to a a standard library output file structure.
     */
    explicit OutputFileAdapter(std::FILE* file) noexcept
        : m_file(file) {
    }

    /**
     * @brief Writes the given buffer to a file.
     * @param [in] buffer A string to write to the file.
     * @return true, if the write was successful.
     * @return false, if the write was not successful.
     */
    bool Write(const std::string& buffer) override {
        return fprintf(m_file, "%s", buffer.c_str()) >= 0;
    }
};

class OutputStreamAdapter : public IOutputAdapter {
private:
    // The associated input stream
    std::ostream* m_stream = nullptr;

public:
    /**
     * @brief Initializes this @ref OutputStreamAdapter instance.
     * @param [in,out] stream Reference to a standard library output file stream.
     */
    explicit OutputStreamAdapter(std::ostream* stream)
        : m_stream(stream) {
    }

    /**
     * @brief Writes the given buffer to a file.
     * @param [in] buffer A string to write to the file.
     * @return true, if the write was successful.
     * @return false, if the write was not successful.
     */
    bool Write(const std::string& buffer) override {
        if (!m_stream)
            return false;

        (*m_stream) << buffer;
        return m_stream->good();
    }
};

/**
 * @brief This class is used for writing saved values to an output file.
 */
class Writer {
private:
    IOutputAdapter* m_adapter = nullptr;

public:
    /**
     * @brief Don't allow default-construction of the @ref Writer class.
     */
    Writer() = delete;

    /**
     * @brief Initializes the underlying @ref m_adapter member.
     * @param [in] file Pointer to a file object adapter.
     */
    explicit Writer(OutputFileAdapter* adapter)
        : m_adapter(adapter) {
    }

    /**
     * @brief Initializes the underlying @ref m_adapter member.
     * @param [in] adapter Pointer to a file stream adapter.
     */
    explicit Writer(OutputStreamAdapter* adapter)
        : m_adapter(adapter) {
    }

    /**
     * @brief Writes given input container values to a file.
     * @param [in] container A container containing @ref Value objects in a named pair structure.
     */
    auto Write(const Container& container) -> bool {
        if (container.empty())
            return m_adapter->Write("# Empty container");

        std::string buffer;
        for (const auto& [name, value] : container) {
            const auto type = value.GetTypeString();
            buffer += name;
            buffer += " = ";
            buffer += value.GetValueString(true);
            buffer += " ; ";
            buffer += type;
            buffer += "\n";
        }

        return m_adapter->Write(buffer);
    }
};
}

#endif // !CONFIGPARSER_WRITER_HPP
