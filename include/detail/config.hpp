/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_DETAIL_CONFIG_HPP
#define CONFIGPARSER_DETAIL_CONFIG_HPP

#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>

/**
 * @brief Determines current architecture we're compiling on by comparing the size of `intptr_t` to either 32-bits or 64-bits.
 */
#if INTPTR_MAX == INT32_MAX
#ifndef PARSER_ARCH_32BIT
#define PARSER_ARCH_32BIT
#endif // !PARSER_ARCH_32BIT
#else
#ifndef PARSER_ARCH_64BIT
#define PARSER_ARCH_64BIT
#endif // !PARSER_ARCH_64BIT
#endif // !INTPTR_MAX == INT32_MAX

/**
 * @brief Sets whether the parser should use the exception handler to catch underlying errors.
 * @note See the @ref parser::ValueHelper structure.
 */
#ifndef PARSER_ENABLE_EXCEPTIONS
#define PARSER_ENABLE_EXCEPTIONS
#endif // !PARSER_ENABLE_EXCEPTIONS

/**
 * @brief Sets whether the parser should check type mismatches in the `Value::ValueContainer` structure.
 * @note Should be sufficient to keep it enabled only in development builds.
 */
#ifndef PARSER_ENABLE_ASSERTIONS
#define PARSER_ENABLE_ASSERTIONS

#include <cassert>

#define PARSER_ENSURE(x) assert((x))
#else
#define PARSER_ENSURE(x)
#endif // !PARSER_ENABLE_ASSERTIONS

namespace parser {
/**
 * @brief Default string type used in the parser.
 */
using StringType = std::string;

/**
 * @brief Default integer type used in the parser.
 * @note You should update the parsing functions in value_helper.hpp accordingly.
 */
using IntegerType = int;

/**
 * @brief Default floating point type used in the parser.
 * @note You should update the parsing functions in value_helper.hpp accordingly.
 */
using FloatType = float;

/**
 * @brief Represents the container in which @ref Value objects should be stored across the @ref Reader and @ref Writer classes.
 */
using Container = std::unordered_map<StringType, struct Value>;
}

#endif // !CONFIGPARSER_DETAIL_CONFIG_HPP
