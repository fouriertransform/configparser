/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_DETAIL_HELPER_HPP
#define CONFIGPARSER_DETAIL_HELPER_HPP

#pragma once

#include <stdexcept>

#include "config.hpp"

namespace parser::detail {
/**
 * @brief Internal wrapper around std::runtime_error.
 */
struct RuntimeError : std::runtime_error {
    using std::runtime_error::runtime_error;
};

/**
 * @brief Custom overload for the @ref Scanner class with additional line info.
 */
struct ScannerError : RuntimeError {
    explicit ScannerError(const std::string& what, int line)
        : RuntimeError("[Line " + std::to_string(line) + "] Scanner error: " + what) {
    }
};

/**
 * @brief Custom overload for the @ref Parser class with additional line info.
 */
struct ParserError : RuntimeError {
    explicit ParserError(const std::string& what, int line)
        : RuntimeError("[Line " + std::to_string(line) + "] Parser error: " + what) {
    }
};

/**
 * @brief Trims whitespace from both the beginning and end of the @p str parameter.
 * @param [in] str A string.
 * @return Returns passed @p str string, stripped of whitespace from the beginning and end.
 */
static std::string Trim(std::string str) {
#ifdef PARSER_ENABLE_EXCEPTIONS
    try {
#endif // !PARSER_ENABLE_EXCEPTIONS
        constexpr static char kWhitespace[7] = "\t\n\v\f\r ";

        const auto not_whitespace = str.find_first_not_of(kWhitespace);
        if (not_whitespace == std::string::npos)
            return "";

        str.erase(0, not_whitespace);

        const auto last_not_whitespace = str.find_last_not_of(kWhitespace);
        if (last_not_whitespace == std::string::npos)
            return str;

        str.erase(last_not_whitespace + 1);
        return str;
#ifdef PARSER_ENABLE_EXCEPTIONS
    } catch (...) {
        // If some exception was raised, then skip this line
        throw RuntimeError("Failed to trim `" + str + "`");
    }
#endif // !PARSER_ENABLE_EXCEPTIONS
}

/**
 * @brief Replaces all occurrences of @p from in @p str to @p to.
 * @param [in] str The input string on which replacement should be done.
 * @param [in] from The term we're trying to replace.
 * @param [in] to The replacement.
 * @return A copy of @p str with all occurrences of @p from changed to @p to.
 */
static std::string ReplaceAll(std::string str, const std::string& from, const std::string& to) {
    std::size_t start_pos = 0u;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}
}

#endif // !CONFIGPARSER_DETAIL_HELPER_HPP
