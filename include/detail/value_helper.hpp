/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_DETAIL_VALUE_HELPER_HPP
#define CONFIGPARSER_DETAIL_VALUE_HELPER_HPP

#pragma once

#include <cmath>
#include <array>

#include "config.hpp"
#include "helper.hpp"

namespace parser {
/**
 * @brief Describes available value types.
 */
enum ValueType : std::uint8_t {
    // Indicates that the Value object hasn't been initialized
    kUndefined,
    // 1 byte
    kBool,
    // Usually sizeof( void* )
    kFloat, kInt,
    // Dynamically allocated, variable-length
    kString, kArray,
    kValueType_Last
};

namespace detail {
/**
 * @brief Number of value types.
 * @note Updates as required.
 */
constexpr static auto kNumValueTypes = kValueType_Last;

/**
 * @brief Represents an array of internal @ref ValueType objects in string format.
 */
constexpr static std::array<const char*, kNumValueTypes> kValueTypes{
    "undefined", "bool", "float", "int", "string", "array"
};

/**
 * @brief Wraps helper functions used for values.
 */
struct ValueHelper {
    /**
     * @brief Attempts to parse the @p str parameter assuming it contains a valid @ref FloatType value.
     * @note This function might throw in case the @p str parameter doesn't contain a valid @ref FloatType value.
     * @param [in] str A string.
     * @return Parsed string represented as a @ref FloatType value.
     */
    static inline auto GetFloat(const std::string& str) -> FloatType {
#ifdef PARSER_ENABLE_EXCEPTIONS
        try {
#endif // !PARSER_ENABLE_EXCEPTIONS
            // or std::stod
            return std::stof(str);
#ifdef PARSER_ENABLE_EXCEPTIONS
        } catch (...) {
            throw RuntimeError("Failed to parse string to float: `" + str + "`");
        }
#endif // !PARSER_ENABLE_EXCEPTIONS
    }

    /**
     * @brief Attempts to parse the @p str parameter assuming it contains a valid @ref IntegerType value.
     * @note This function might throw in case the @p str parameter doesn't contain a valid @ref IntegerType value.
     * @note This function supports both the decimal and hexadecimal format, including rounding of floating point values.
     * i.e. 13.37 will be rounded to 13
     * i.e. 133.7 will be rounded to 134
     * i.e. 0x7FFFFFFF, 0xD34DB33F, etc.
     * @param [in] str A string.
     * @return Parsed string represented as a @ref IntegerType type.
     */
    static inline auto GetInt(const std::string& str) -> IntegerType {
#ifdef PARSER_ENABLE_EXCEPTIONS
        try {
#endif // !PARSER_ENABLE_EXCEPTIONS
            if (str.size() > 2) {
                if (str[0] == '0' && str[1] == 'x') {
                    // This is a hexadecimal value, set appropriate base
                    // and cut first two letters
                    auto i = std::stoul(str.substr(2), nullptr, 16);
                    return static_cast< IntegerType >( i );
                } else if (str.find('.') != std::string::npos) {
                    // This is a decimal value (but will be parsed as integer
                    // so pass this to GetFloat instead
                    return static_cast< IntegerType >( std::round(GetFloat(str)) );
                }
            }
            // or std::stoll
            return std::stoi(str);
#ifdef PARSER_ENABLE_EXCEPTIONS
        } catch (...) {
            throw RuntimeError("Failed to parse string to integer: `" + str + "`");
        }
#endif // !PARSER_ENABLE_EXCEPTIONS
    }

    /**
     * @brief Attempts to convert an input string to a boolean representation.
     * @param [in] str A string.
     * @return true if str is a representation of a boolean truthy value, else false
     */
    static inline auto GetBool(const std::string& str) noexcept -> bool {
        return str == "true" || str == "1";
    }

    /**
     * @brief Attempts to determine the @ref ValueType value of the @p type parameter.
     * @param [in] type A string.
     * @return The @ref ValueType value corresponding to the @p type parameter.
     */
    static auto FromString(const std::string& type) -> ValueType {
        // Is type equal to...?
        constexpr static auto kArrayIndex = static_cast< std::size_t >( kArray );
        for (auto i = 0u; i < kArrayIndex; ++i) {
            if (type == kValueTypes[i])
                return static_cast< ValueType >( i );
        }

        // Does type start with `array`?
        if (!type.find(kValueTypes[kArrayIndex]))
            return kArray;

#ifdef PARSER_ENABLE_EXCEPTIONS
        throw RuntimeError("Failed to parse value type: `" + type + "`");
#else
        // We didn't find anything
        return kUndefined;
#endif // !PARSER_ENABLE_EXCEPTIONS
    }

    /**
     * @brief Attempts to determine convert an input enum-based value type into a string.
     * @param [in] type A string.
     * @return The @ref ValueType value corresponding to the @p type parameter.
     */
    static auto ToString(const ValueType type) -> const char* {
        const auto index = static_cast< std::size_t >( type );
        if (index > kValueTypes.size())
            return kValueTypes[0];
        return kValueTypes[index];
    }
};
}
}

#endif // !CONFIGPARSER_DETAIL_VALUE_HELPER_HPP
