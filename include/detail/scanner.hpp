/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_DETAIL_SCANNER_HPP
#define CONFIGPARSER_DETAIL_SCANNER_HPP

#pragma once

#include <algorithm>

#include "token.hpp"
#include "helper.hpp"
#include "value_helper.hpp"

namespace parser {
namespace {
/**
 * @brief Represents a keyword string and its @p TokenType as a pair.
 */
using KeywordPair = std::pair<const char*, TokenType>;

/**
 * @brief Represents the list of reserved identifier keywords.
 */
constexpr std::array<KeywordPair, 2> kReservedKeywords{ {
                                                            { "true", TokenType::kTrue },
                                                            { "false", TokenType::kFalse }
                                                        } };
}

/**
 * @brief This class scans a given input string and tries to represent
 * its contents as a stream of @p Token objects. Also called a 'tokenizer'.
 */
class Scanner {
private:
    /**
     * @brief The original input string.
     */
    const std::string_view m_source;

    /**
     * @brief The current positions we're evaluating/scanning over.
     */
    std::string_view::const_iterator m_begin, m_end;

    /**
     * @brief The current line we're on. Useful for reporting error messages.
     */
    int m_current_line = 0;

    /**
     * @brief The current token that we've just scanned.
     */
    Token m_token;

private:

    /**
     * @brief Creates a token object with the given token type.
     * @param [in] token_type The token type of this @p Token object.
     * @return A new @p Token object with the given token type.
     */
    [[nodiscard]] inline auto MakeToken(TokenType token_type) const -> Token {
        return Token{ token_type, { m_begin, m_end }, m_current_line };
    }

    /**
     * @brief Creates a token object with the given token type and lexeme.
     * @param [in] token_type The token type for this token.
     * @param [in] value The lexeme value for this token.
     * @return A new @p Token object with the given token type and lexeme.
     */
    [[nodiscard]] inline auto MakeToken(TokenType token_type, std::string&& value) const -> Token {
        return Token{ token_type, std::move(value), m_current_line };
    }

public:
    /**
     * @brief Initializes a new @p Scanner instance.
     * @param [in] source The input string to scan.
     */
    explicit Scanner(std::string_view source)
        : m_source(source)
          , m_begin(source.cbegin())
          , m_end(source.cbegin())
          , m_current_line(0)
          , m_token(ConsumeToken()) {
    }

    /**
     * @brief Tries to create a string token.
     * @return A new token with type @p TokenType::kString
     */
    auto ConsumeString() -> Token {
        std::string lexeme;

        while (m_end != m_source.end()) {
            const auto prev_ch = *m_end;
            ++m_end;

            const auto at_end = *m_end == '"';
            const auto escaped = prev_ch == '\\';

            if (at_end && escaped)
                continue;

            switch (prev_ch) {
            case '\r':
                break;
            case '\n':
                ++m_current_line;
                [[fallthrough]];
            default:
                lexeme += prev_ch;
                break;
            }

            if (at_end)
                break;
        }

        // Check unterminated string
        if (m_end == m_source.end())
            throw detail::ScannerError{ "Unterminated string", m_current_line };

        // The closing "
        ++m_end;

        // Trim surrounding quotes and normalize line endings for literal value
        return MakeToken(TokenType::kString, std::move(lexeme));
    }

    /**
     * @brief Tries to create a number token.
     * @return A new token with type @p TokenType::kNumber
     */
    auto ConsumeNumber() -> Token {
        auto is_hex = false;
        if (*m_end == '-' || *m_end == '+') {
            ++m_end;
        } else {
            // Consume "0x" prefix for hexadecimal values
            is_hex = *m_begin == '0' && *m_end == 'x';
            if (is_hex)
                ++m_end;
        }

        auto hex_counter = 0u;
        while (m_end != m_source.end()) {
            if (is_hex) {
                const auto ch = std::tolower(*m_end);

                auto okay = std::isdigit(ch);
                switch (ch) {
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                    okay = true;
                    break;
                default:
                    break;
                }

                if (!okay)
                    break;

                ++hex_counter;
            } else {
                if (!std::isdigit(*m_end))
                    break;
            }
            ++m_end;
        }

        constexpr static auto kMaxHexDigits = sizeof(IntegerType) * 2;
        if (hex_counter > kMaxHexDigits) {
            throw detail::ScannerError(
                "Hex value cannot have more than " + std::to_string(kMaxHexDigits) + " digits", m_current_line
            );
        }

        // Look for a fractional part
        if (
            !is_hex
            && m_end != m_source.end() && *m_end == '.'
            && (m_end + 1) != m_source.end() && std::isdigit(*(m_end + 1))
            ) {
            // Consume the "." and one digit
            m_end += 2;

            while (m_end != m_source.end() && std::isdigit(*m_end))
                ++m_end;
        }

        return MakeToken(TokenType::kNumber, std::string{ m_begin, m_end });
    }

    /**
     * @brief Tries to create an identifier token from a given string.
     * @return A new token with type @p TokenType::kIdentifier
     * (or other if this is a reserved keyword)
     */
    auto ConsumeIdentifier() -> Token {
        static const auto is_valid_ch = [](char ch) -> bool {
            return std::isalnum(ch) || ch == '_' || ch == '.';
        };

        while (m_end != m_source.end() && is_valid_ch(*m_end))
            ++m_end;

        std::string identifier{ m_begin, m_end };
        std::transform(
            identifier.begin(), identifier.end(), identifier.begin(), ::tolower
        );

        const auto* found = std::find_if(
            kReservedKeywords.cbegin(), kReservedKeywords.cend(), [&](const KeywordPair& pair) -> bool {
                return pair.first == identifier;
            }
        );

        if (found != kReservedKeywords.cend())
            return MakeToken(found->second);

        return MakeToken(TokenType::kIdentifier);
    }

    /**
     * @brief Tries its best to consume a token.
     * @return The next token from the input string.
     */
    auto ConsumeToken() -> Token {
        // Loop because we might skip some tokens
        for (; m_end != m_source.end(); m_begin = m_end) {
            const auto ch = *m_end;
            ++m_end;

            switch (ch) {
                // Whitespace
            case '\n':
                ++m_current_line;
                continue;
                // Comment
            case '#':
                // A comment goes until the end of the line
                while (m_end != m_source.end() && *m_end != '\n')
                    ++m_end;
                continue;
                // We're at the end
            case '\0':
                break;
                // Single char tokens
            case '[':
                return MakeToken(TokenType::kLeftBracket);
            case ']':
                return MakeToken(TokenType::kRightBracket);
            case ';':
                return MakeToken(TokenType::kSemicolon);
            case '=':
                return MakeToken(TokenType::kEqual);
            case '<':
                return MakeToken(TokenType::kLesser);
            case '>':
                return MakeToken(TokenType::kGreater);
            case ',':
                return MakeToken(TokenType::kComma);
                // Literals and Keywords
            case '"':
                return ConsumeString();
            case '-':
            case '+':
                if (std::isdigit(*m_end))
                    return ConsumeNumber();
                [[fallthrough]];
            default:
                if (std::isspace(ch)) {
                    continue;
                } else if (std::isdigit(ch)) {
                    return ConsumeNumber();
                } else if (std::isalpha(ch) || ch == '_') {
                    return ConsumeIdentifier();
                }

                throw detail::ScannerError("Unexpected character encountered", m_current_line);
            }
        }

        // The final token is always EOF
        return Token{ TokenType::kEOF, m_current_line };
    }

    /**
     * @brief Advances the token iterator over to the next token.
     */
    void Advance() {
        if (m_begin != m_source.end()) {
            // We are at the beginning of the next lexeme
            m_begin = m_end;
            m_token = ConsumeToken();
        } else {
            // We're at the end, nothing to do.
        }
    }

    /**
     * @return The current token.
     */
    [[nodiscard]] inline auto GetToken() const -> const Token& {
        return m_token;
    }

    /**
     * @return The reference to the current token.
     */
    inline auto GetTokenRef() -> Token& {
        return m_token;
    }

    /**
     * @return The line on which the current token is (or starts).
     */
    [[nodiscard]] inline auto GetLine() const -> int {
        return m_token.GetLine();
    }

    /**
     * @return The type of the current token.
     */
    [[nodiscard]] inline auto GetType() const -> TokenType {
        return m_token.GetType();
    }

    /**
     * @return The lexeme of the current token.
     */
    [[nodiscard]] inline auto GetLexeme() const -> const std::string& {
        return m_token.GetLexeme();
    }

    /**
     * @return The starting position for the current token in the string source input.
     */
    [[nodiscard]] inline auto GetStart() const -> std::string_view::const_iterator {
        return m_begin;
    }

    /**
	 * @return The ending position for the current token in the string source input.
     */
    [[nodiscard]] inline auto GetEnd() const -> std::string_view::const_iterator {
        return m_end;
    }
};
}

#endif // !CONFIGPARSER_DETAIL_SCANNER_HPP
