/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_VALUE_HPP
#define CONFIGPARSER_VALUE_HPP

#pragma once

#include <string>
#include <vector>
#include <cmath>

#include "value_helper.hpp"

namespace {
template<typename T>
using AllocatorType = std::allocator<T>;

template<typename T>
using AllocatorTraits = std::allocator_traits<AllocatorType<T>>;

/**
 * @brief Helper for exception-safe object creation
 */
template<typename T, typename... Args>
T* AllocateObject(Args&& ... args) {
    AllocatorType<T> allocator;

    auto deleter = [&](T* object) {
        AllocatorTraits<T>::deallocate(allocator, object, 1);
    };

    std::unique_ptr<T, decltype(deleter)> object(
        AllocatorTraits<T>::allocate(allocator, 1), deleter
    );

    AllocatorTraits<T>::construct(
        allocator, object.get(), std::forward<Args>(args)...
    );

    PARSER_ENSURE(object != nullptr);
    return object.release();
}
}

namespace parser {
/**
 * @brief Represents the array type used internally by the parser.
 */
using ArrayType = std::vector<struct Value, AllocatorType<struct Value>>;

/**
 * @brief Acts as a wrapper over the underlying ValueContainer union.
 * @note On x64 there will be a 7 byte alignment boundary padding by the compiler.
 * @note The size of this structure is sizeof( void* ) + sizeof( std::uint8_t ), which is 9 bytes on x64.
 */
struct Value {
    /**
     * @brief Represents a value object which can hold multiple types of data.
     * @note Variable-length types are stored as pointers to save memory.
     * @note The size of the union should not exceed sizeof( void* ).
     */
    union ValueContainer {
        ArrayType* m_array;
        StringType* m_string;
        bool m_bool;
        IntegerType m_int;
        FloatType m_float;

        /**
         * @brief Default constructor
         */
        ValueContainer() = default;

        /**
         * @brief Constructor for the boolean type.
         * @param [in] value A boolean value.
         */
        ValueContainer(const bool value) noexcept : m_bool(value) {
        }

        /**
         * @brief Constructor for the @ref IntegerType type.
         * @param [in] value A @ref IntegerType value.
         */
        ValueContainer(const IntegerType value) noexcept : m_int(value) {
        }

        /**
        * @brief Constructor for the @ref FloatType type.
        * @param [in] value A @ref FloatType value.
        */
        ValueContainer(const FloatType value) noexcept : m_float(value) {
        }

        /**
         * @brief Constructor for empty values based on the @p type parameter.
         * @param [in] type Type of the object to be created as.
         */
        ValueContainer(const ValueType type) {
            switch (type) {
            case kArray:
                m_array = AllocateObject<ArrayType>();
                return;
            case kString:
                m_string = AllocateObject<StringType>("");
                return;
            case kBool:
                m_bool = false;
                return;
            case kInt:
                m_int = IntegerType(0);
                return;
            case kFloat:
                m_float = FloatType(0);
                return;
            default:
                return;
            }
        }

        /**
         * @brief Constructor for the @ref StringType type.
         * @param [in] value A const reference @ref StringType value.
         */
        ValueContainer(const StringType& value)
            : m_string(AllocateObject<StringType>(value)) {
        }

        /**
        * @brief Constructor for the @ref StringType type.
        * @param [in] value A rvalue @ref StringType value.
        */
        ValueContainer(StringType&& value)
            : m_string(AllocateObject<StringType>(std::move(value))) {
        }

        /**
         * @brief Constructor for the @ref ArrayType type.
         * @param [in] value A const reference @ref ArrayType value.
         */
        ValueContainer(const ArrayType& value)
            : m_array(AllocateObject<ArrayType>(value)) {
        }

        /**
         * @brief Constructor for the @ref ArrayType type.
         * @param [in] value A rvalue @ref ArrayType value.
         */
        ValueContainer(ArrayType&& value)
            : m_array(AllocateObject<ArrayType>(std::move(value))) {
        }

        /**
         * @brief Deallocates any allocations performed by this object.
         * @param [in] type Current object type.
         */
        auto Destroy(ValueType value_type) noexcept -> void {
            switch (value_type) {
            case kArray: {
                AllocatorType<ArrayType> allocator;
                AllocatorTraits<ArrayType>::destroy(allocator, m_array);
                AllocatorTraits<ArrayType>::deallocate(allocator, m_array, 1);
                m_array = nullptr;
                return;
            }
            case kString: {
                AllocatorType<StringType> allocator;
                AllocatorTraits<StringType>::destroy(allocator, m_string);
                AllocatorTraits<StringType>::deallocate(allocator, m_string, 1);
                m_string = nullptr;
                return;
            }
            default:
                return;
            }
        }

        /**
         * @brief A helper method for niftily obtaining the current float value.
         * @return The pointer to the floating value.
         * @note Does not check the current value type.
         */
        inline auto Get(FloatType* /*unused*/ ) -> FloatType* {
            return &m_float;
        }

        /**
         * @brief A helper method for niftily obtaining the current integer value.
         * @return The pointer to the integer value.
         * @note Does not check the current value type.
         */
        inline auto Get(IntegerType* /*unused*/ ) -> IntegerType* {
            return &m_int;
        }

        /**
         * @brief A helper method for niftily obtaining the current boolean value.
         * @return The pointer to the boolean value.
         * @note Does not check the current value type.
         */
        inline auto Get(bool* /*unused*/ ) -> bool* {
            return &m_bool;
        }

        /**
         * @brief A helper method for niftily obtaining the current string value.
         * @return The pointer to the string value.
         * @note Does not check the current value type.
         */
        inline auto Get(StringType* /*unused*/ ) -> StringType* {
            return m_string;
        }

        /**
         * @brief A helper method for niftily obtaining the current array value.
         * @return The pointer to the array value.
         * @note Does not check the current value type.
         */
        inline auto Get(ArrayType* /*unused*/ ) -> ArrayType* {
            return m_array;
        }
    };

    /**
     * @brief Default-constructs a @ref Value object.
     */
    Value() = default;

    /**
     * @brief Destroys the current @ref Value object and frees all allocated memory.
     */
    ~Value() {
        AssertInvariant();
        m_value.Destroy(m_type);
    }

    /**
     * @brief Constructs an empty object based on @p type.
     * @param [in] type The type of the value to create.
     */
    Value(const ValueType value_type) noexcept
        : m_value(value_type)
          , m_type(value_type) {
        AssertInvariant();
    }

    /**
     * @brief Wrapper around @ref ValueContainer::ValueContainer for boolean values.
     */
    Value(const bool value) noexcept
        : m_value(value)
          , m_type(kBool) {
        AssertInvariant();
    }

    /**
     * @brief Wrapper around @ref ValueContainer::ValueContainer for @ref FloatType values.
     */
    Value(const FloatType value) noexcept
        : m_value(value)
          , m_type(kFloat) {
        AssertInvariant();
    }

    /**
     * @brief Wrapper around @ref ValueContainer::ValueContainer for @ref IntegerType values.
     */
    Value(const IntegerType value) noexcept
        : m_value(value)
          , m_type(kInt) {
        AssertInvariant();
    }

    /**
     * @brief Wrapper around @ref ValueContainer::ValueContainer for @ref StringType values.
     */
    Value(StringType value) noexcept
        : m_value(std::forward<StringType>(value))
          , m_type(kString) {
        AssertInvariant();
    }

    /**
     * @brief Wrapper around the @ref StringType constructor for the underlying literal type of @ref StringType.
     */
    Value(const typename StringType::value_type* value) noexcept
        : Value(StringType{ value }) {
    }

    /**
     * @brief Wrapper around @ref ValueContainer::ValueContainer for @ref ArrayType values.
     */
    Value(ArrayType value) noexcept
        : m_value(std::forward<ArrayType>(value))
          , m_type(kArray) {
        AssertInvariant();
    }

    /**
     * @brief Move constructor that swaps values from the @p other parameter.
     * @param [in,out] other The other value used to move to this object.
     */
    Value(Value&& other) noexcept
        : m_value(other.m_value)
          , m_type(other.m_type) {
        // Check that passed value is valid
        other.AssertInvariant();

        // Invalidate
        other.m_type = kUndefined;
        other.m_value = {};

        AssertInvariant();
    }

    /**
     * @brief Copy constructor that copies values from the @p other parameter.
     * @param [in] other The value to copy from.
     */
    Value(const Value& other) noexcept
        : m_type(other.m_type) {
        // Check that passed value type is valid
        other.AssertInvariant();

        // Copy value
        switch (other.m_type) {
        case kBool:
            m_value = other.m_value.m_bool;
            break;
        case kFloat:
            m_value = other.m_value.m_float;
            break;
        case kInt:
            m_value = other.m_value.m_int;
            break;
        case kString:
            m_value = *other.m_value.m_string;
            break;
        case kArray:
            m_value = *other.m_value.m_array;
            break;
        default:
            break;
        }

        // Check that everything internally is valid
        AssertInvariant();
    }

    /**
     * @brief Used for pretty-printing the current value's type recursively.
     */
    [[nodiscard]] auto GetTypeString() const -> StringType {
        using detail::ValueHelper;

        auto type = StringType(ValueHelper::ToString(m_type));
        if (m_type == kArray) {
            auto* arr = m_value.m_array;
            if (arr && !arr->empty()) {
                type += "<";
                type += arr->at(0).GetTypeString();
                type += ">";
                return type;
            } else {
                return type + "<>";
            }
        }

        return type;
    }

    /**
     * @brief Used for pretty-printing the internal value.
     * @param [in] escaped Whether the string value should be escaped.
     * @note @ref Address elements are represented in decimal form.
     */
    [[nodiscard]] auto GetValueString(bool escaped = false) const -> StringType {
        switch (m_type) {
        case kBool:
            return m_value.m_bool ? "true" : "false";
        case kFloat:
            return std::to_string(m_value.m_float);
        case kInt:
            return std::to_string(m_value.m_int);
        case kArray: {
            StringType out;

            auto is_first = true;
            for (const auto& value : *m_value.m_array) {
                if (is_first)
                    is_first = false;
                else
                    out += ", ";

                out += value.GetValueString(escaped);
            }

            return '[' + out + ']';
        }
        case kString: {
            if (escaped) {
                return "\"" + detail::ReplaceAll(*m_value.m_string, "\"", "\\\"") + "\"";
            } else {
                return "\"" + *m_value.m_string + "\"";
            }
        }
        default:
            return "undefined";
        }
    }

    /**
     * @brief Used for checking internal invariants.
     */
    inline auto AssertInvariant() const noexcept -> void {
        PARSER_ENSURE(m_type != kArray || m_value.m_array != nullptr);
        PARSER_ENSURE(m_type != kString || m_value.m_string != nullptr);
    }

    /**
     * @brief Copy assignment operator that copies via swapping.
     * @param [in] other The value to copy from.
     */
    Value& operator=(Value other) noexcept(
    std::is_nothrow_move_constructible<ValueContainer>::value
    ) {
        // Check that passed value is valid
        other.AssertInvariant();

        // Perform swapping on type/value
        std::swap(m_type, other.m_type);
        std::swap(m_value, other.m_value);

        return *this;
    }

    /**
     * @brief Gives convenient access to array objects.
     * @param [in] i The index of the entry we're trying to access in the current value object.
     * @return The pointer to the entry we're trying to access.
     */
    auto operator[](std::ptrdiff_t i) const -> Value* {
        if (m_type == kArray)
            return &m_value.m_array->at(i);
        return nullptr;
    }

    /**
     * @brief Gives a convenient accessor to the object's value based on the input type.
     * @tparam T The type of the value we're trying to fetch.
     * @return The value of this object, which is of the same type as the template.
     * @note The compiler should optimize all of the @p Get helper methods
     * since all pointers will point to the same memory location and
     * this will just evaluate to syntactic sugar in the end.
     * @note Does not check whether the templated value and the represented value match.
     */
    template<typename T>
    inline auto Get() -> T {
        return *m_value.Get(static_cast< T* >( nullptr ));
    }

    /**
     * @brief Gives a convenient accessor to the object's value pointer based on the input type.
     * @tparam T The type of the value we're trying to fetch.
     * @return The pointer to the value of this object, which is of the same type as the template.
     * @note The compiler should optimize all of the @p Get helper methods
     * since all pointers will point to the same memory location and
     * this will just evaluate to syntactic sugar in the end.
     * @note Does not check whether the templated value and the represented value match.
     */
    template<typename T>
    inline auto GetPtr() -> T* {
        return m_value.Get(static_cast< T* >( nullptr ));
    }

    /**
     * @brief Convenient accessor for array size.
     * @return Array size if this value is an array, otherwise 0.
     */
    [[nodiscard]] inline auto GetSize() const -> std::size_t {
        if (m_type == kArray)
            return m_value.m_array->size();
        return 0u;
    }

    /**
     * @brief Checks whether this value is a scalar (bool, float, int or undefined)
     * @return true if type is scalar, else false.
     */
    [[nodiscard]] inline auto IsScalar() const -> bool {
        return m_type < kString;
    }

    /**
     * @brief The container interface for the current value.
     */
    ValueContainer m_value{};

    /**
     * @brief The type of the current value.
     */
    ValueType m_type = kUndefined;
};

/**
 * @brief Sanity check to make sure we're not wasting memory.
 */
static_assert(
    sizeof(Value::ValueContainer) == sizeof(void*), "ValueContainer is not sizeof( void* ) bytes!"
);
}

#endif // !CONFIGPARSER_VALUE_HPP
