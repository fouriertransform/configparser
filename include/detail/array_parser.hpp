/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_DETAIL_ARRAY_PARSER_HPP
#define CONFIGPARSER_DETAIL_ARRAY_PARSER_HPP

#pragma once

#include "scanner.hpp"
#include "value_helper.hpp"
#include "value.hpp"

namespace parser {
/**
 * @brief Parses and verifies an array value using recursion.
 */
class ArrayParser {
private:
    /**
     * @brief Pointer to the current @p Scanner object.
     */
    Scanner* m_scanner = nullptr;

    /**
     * @brief The current line we're on. (Arrays can span multiple lines)
     */
    int m_current_line = 0;

    /**
     * @brief Type which represents the value types in order of occurrence.
     * Used for verifying homogeneity of an array.
     */
    using TypeNodes = std::vector<ValueType>;

    /**
     * @brief Represents the current context during type parsing.
     * Useful since we're using recursion.
     */
    struct TypeParseContext {
        std::string_view::const_iterator m_start{};
        int m_level = 0;
        bool m_found_close_tag = false;
        bool m_found_start_tag = false;
    };

private:

    /**
     * @brief Recursively parses an array type string.
     * @param [in,out] ctx The current context for parsing the current type.
     * @param [in,out] type_nodes The list used for keeping track of types we've seen so far.
     */
    auto AcquireArraySubtype(TypeParseContext& ctx, TypeNodes& type_nodes) -> void {
        while (m_scanner->GetType() != TokenType::kEOF) {
            switch (m_scanner->GetType()) {
            case TokenType::kLesser: {
                ctx.m_found_start_tag = true;

                if (ctx.m_found_close_tag) {
                    throw detail::ParserError(
                        "Array open tag can't come after a close tag", m_current_line
                    );
                }

                ++ctx.m_start;
                ++ctx.m_level;

                m_scanner->Advance();
                return AcquireArraySubtype(ctx, type_nodes);
            }
            case TokenType::kGreater: {
                if (ctx.m_level <= 0) {
                    throw detail::ParserError(
                        "Array close tag can't come before an open tag", m_current_line
                    );
                }

                ctx.m_found_close_tag = true;
                --ctx.m_level;

                m_scanner->Advance();

                if (!ctx.m_level) {
                    // Peek next token to see whether it's a mistake
                    // and throw a better error message, than would be
                    if (m_scanner->GetType() == TokenType::kGreater) {
                        throw detail::ParserError(
                            "Array type has already been fully closed", m_current_line
                        );
                    } else {
                        return;
                    }
                }

                continue;
            }
            case TokenType::kIdentifier: {
                using detail::ValueHelper;

                const auto type = ValueHelper::FromString(m_scanner->GetLexeme());
                if (type != kUndefined) {
                    type_nodes.emplace_back(type);
                    m_scanner->Advance();
                    continue;
                }

                [[fallthrough]];
            }
            default:
                throw detail::ParserError(
                    "Invalid array value type received", m_current_line
                );
            }
        }
    }

    /**
     * @brief Acquires the current array type with all of its subtypes.
     * @return The array types (and also its subtypes) represented as a std::vector.
     */
    auto GetArrayType() -> TypeNodes {
        auto ctx = TypeParseContext{
            .m_start = m_scanner->GetStart()
        };

        TypeNodes type_nodes;
        AcquireArraySubtype(ctx, type_nodes);

        if (!ctx.m_found_start_tag) {
            throw detail::ParserError(
                "The array type requires open/close brackets, regardless of subtype presence", m_current_line
            );
        }

        if (ctx.m_level != 0) {
            throw detail::ParserError(
                "Uneven number of open/close brackets on array type", m_current_line
            );
        }

        return type_nodes;
    }

    /**
     * @brief Parses the value of an array in accordance to the declared type nodes.
     * @param [in] input_array The array (of strings) we're parsing
     * @param [out] output_array The array (of @p Value objects) we've parsed so far.
     * @param [in] type_nodes List of array types in order of occurrence.
     * @param [in] node_index The node type in the @p type_nodes vector, we're currently parsing for.
     */
    auto PopulateArray(
        ArrayType* input_array, ArrayType& output_array, const TypeNodes& type_nodes, const std::size_t node_index = 0
    ) -> void {
        if (!input_array)
            return;

        if (node_index >= type_nodes.size())
            return;

        const auto type_node = type_nodes[node_index];
        for (const auto& value : *input_array) {
            if (
                (value.m_type == kArray && type_node != kArray) ||
                (value.m_type != kArray && type_node == kArray)
                ) {
                throw detail::ParserError(
                    "Mismatch between actual value and declared type", m_current_line
                );
            }

            // Handle nested arrays
            if (type_node == kArray) {
                PARSER_ENSURE(value.m_value.m_array != nullptr);

                ArrayType temporary_array;
                PopulateArray(
                    value.m_value.m_array, temporary_array, type_nodes, node_index + 1
                );

                output_array.emplace_back(temporary_array);
                continue;
            }

            PARSER_ENSURE(value.m_type == kString && value.m_value.m_string != nullptr);

            using detail::ValueHelper;

            const auto str = std::move(*value.m_value.m_string);
            switch (type_node) {
            case kFloat:
                output_array.emplace_back(ValueHelper::GetFloat(str));
                break;
            case kInt:
                output_array.emplace_back(ValueHelper::GetInt(str));
                break;
            case kBool:
                output_array.emplace_back(ValueHelper::GetBool(str));
                break;
            case kString:
                output_array.emplace_back(str);
                break;
            default:
                throw detail::ParserError(
                    "Failed to convert value based on given array token type", m_current_line
                );
            }
        }
    }

    /**
     * @brief Verifies the size consistency of a vector.
     * @param [in,out] sizes The list of array sizes as they're encountered.
     * @param [in] value The current array value we're verifying.
     * @param [in] type_nodes The list of array types in order of occurrence.
     * @param [in] node_index The index of the current type we're currently verifying (against the @p value value).
     * @return
     */
    auto CheckContainerConsistencyImpl(
        std::vector<int>& sizes, const Value& value, const TypeNodes& type_nodes, const std::size_t node_index = 0
    ) const -> bool {
        if (node_index >= type_nodes.size())
            return false;

        const auto type_node = type_nodes[node_index];
        if (value.m_type != type_node)
            return false;

        if (type_node == kArray) {
            PARSER_ENSURE(value.m_value.m_array != nullptr);

            // Recurse first
            for (const auto& v : *value.m_value.m_array) {
                if (!CheckContainerConsistencyImpl(sizes, v, type_nodes, node_index + 1))
                    return false;
            }

            // Check top-down
            auto& size = sizes[node_index];
            if (size == -1) {
                size = static_cast< int >( value.m_value.m_array->size() );
            } else if (static_cast< std::size_t >( size ) != value.m_value.m_array->size()) {
                throw detail::ParserError(
                    "Inconsistent array sizes on a given depth aren't allowed", m_current_line
                );
            }
        }

        return true;
    }

    /**
     * @brief Verifies the size consistency of a vector.
     * @param [in] parsed_array The array we've parsed.
     * @param [in] type_nodes The list of array types in order of occurrence.
     */
    auto CheckContainerConsistency(
        const ArrayType& parsed_array, const TypeNodes& type_nodes
    ) const -> void {
        if (parsed_array.empty() || type_nodes.empty())
            return;

        std::vector<int> sizes;
        sizes.resize(type_nodes.size());

        for (auto& size : sizes)
            size = -1;

        for (const auto& value : parsed_array) {
            if (!CheckContainerConsistencyImpl(sizes, value, type_nodes))
                throw detail::ParserError("Failed to ensure array homogeneity", m_current_line);
        }
    }

public:
    /**
     * @brief Initializes a new array parser instance.
     * @param [in] scanner The pointer to the current @p Scanner instance.
     */
    explicit ArrayParser(Scanner* scanner)
        : m_scanner(scanner)
          , m_current_line(m_scanner->GetLine()) {
    }

    /**
     * @brief Performs parsing and validation on an intermediate array representation
     * @param [in] scanned_array The array (of strings) we're parsing and validating
     * @return The fully parsed and validated array based on @p scanned_array
     */
    auto GetArrayValue(ArrayType* scanned_array) -> ArrayType {
        ArrayType parsed_array;

        // If there is no subtype specified, add this variable as an empty array
        auto type_nodes = GetArrayType();
        if (type_nodes.empty())
            return parsed_array;

        // Parse and populate parsed_array with actual values
        PopulateArray(scanned_array, parsed_array, type_nodes);

        // Check parsed array for type mismatches and/or size inconsistencies
        CheckContainerConsistency(parsed_array, type_nodes);

        return parsed_array;
    }

    /**
     * @brief Consumes incoming array tokens until it encounters
     * the end of the input array.
     * The consumed token lexemes are stored inside @p scanned_array
     * as an intermediate representation.
     * @param [out] scanned_array The intermediate array representation we've scanned so far.
     * @note This function is called after we've already found an occurrence
     * of kLeftBracket.
     * @note This function can only proudce kString or kArray entries
     * which we will later check in @p PopulateArray.
     */
    auto ConsumeArrayValue(ArrayType& scanned_array) -> void {
        auto last_comment = -1;
        while (m_scanner->GetType() != TokenType::kEOF) {
            if (m_scanner->GetLine() <= last_comment) {
                m_scanner->Advance();
                continue;
            }

            switch (m_scanner->GetType()) {
            case TokenType::kHash: {
                last_comment = m_scanner->GetLine();
                continue;
            }
            case TokenType::kComma:
                m_scanner->Advance();
                continue;
            case TokenType::kNumber:
            case TokenType::kString:
            case TokenType::kTrue:
            case TokenType::kFalse:
                scanned_array.emplace_back(m_scanner->GetLexeme());
                m_scanner->Advance();
                continue;
            case TokenType::kLeftBracket: {
                m_scanner->Advance();

                ArrayType nested_array;
                ConsumeArrayValue(nested_array);

                scanned_array.emplace_back(nested_array);
                continue;
            }
            case TokenType::kRightBracket:
                m_scanner->Advance();
                return;
            default:
                throw detail::ParserError("Unexpected array value encountered", m_current_line);
            }
        }
    }
};
}

#endif // !CONFIGPARSER_DETAIL_ARRAY_PARSER_HPP
