/*
 * ConfigParser - Lightweight parser for a simple and intuitive key/value/type configuration format.
 * Copyright (C) 2018-2023  Branko Paunović <branko.paunovic@protonmail.ch>
 *
 * This file is part of ConfigParser.
 *
 * ConfigParser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ConfigParser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ConfigParser.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONFIGPARSER_DETAIL_TOKEN_HPP
#define CONFIGPARSER_DETAIL_TOKEN_HPP

#pragma once

#include <array>
#include <string>
#include <variant>
#include <optional>

namespace parser {
/**
 * @brief The list of available tokens.
 */
enum class TokenType {
    // Default value. Should always be ignored.
    kIgnore = 0,

    // Single-character tokens
    kLeftBracket, kRightBracket,
    kEqual, kLesser, kGreater,
    kSemicolon, kHash, kComma,

    // Literals
    kIdentifier, kString, kNumber,

    // Keywords
    kTrue, kFalse,

    // Last token
    kEOF
};

/**
 * @brief The total number of tokens.
 */
constexpr static auto kNumTokens = static_cast< std::size_t >( TokenType::kEOF );

/**
 * @brief Maps a token type to a string.
 * @param [in] token_type A token type.
 * @return The string representation of this token type.
 */
[[maybe_unused]] static auto GetTokenTypeAsString(TokenType token_type) -> const char* {
    constexpr static std::array<const char*, kNumTokens + 1> kTokenStrings{
        "ignore",
        "l_bracket", "r_bracket",
        "equal", "lesser", "greater",
        "semicolon", "hash", "comma",
        "identifier", "string", "number",
        "true", "false",
        "eof"
    };

    auto index = static_cast< std::size_t >( token_type );
    if (index > kNumTokens)
        index = 0;

    return kTokenStrings[index];
}

/**
 * @brief Represents a generalist token object, which is
 * differentiated by its type, an optional lexeme and the line it was seen on.
 */
class Token {
private:
    TokenType m_type;
    std::string m_lexeme;

    int m_line = 0;

public:
    Token(TokenType type, int line)
        : m_type(type)
          , m_line(line) {
    }

    Token(TokenType type, std::string&& lexeme, int line)
        : m_type(type)
          , m_lexeme(std::move(lexeme))
          , m_line(line) {
    }

    [[nodiscard]] inline auto GetType() const -> TokenType {
        return m_type;
    }

    [[nodiscard]] inline auto GetLexeme() const -> const std::string& {
        return m_lexeme;
    }

    [[nodiscard]] inline auto GetLine() const -> int {
        return m_line;
    }

    inline auto SetLexeme(std::string&& lexeme) -> void {
        m_lexeme = std::move(lexeme);
    }
};

inline auto operator==(const Token& lhs, TokenType rhs) -> bool {
    return lhs.GetType() == rhs;
}

inline auto operator!=(const Token& lhs, TokenType rhs) -> bool {
    return lhs.GetType() != rhs;
}
}

#endif // !CONFIGPARSER_DETAIL_TOKEN_HPP
